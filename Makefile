RED = "\033[1;31m"
GREEN = '\033[1;32m'
YELLOW = '\033[1;33m'
NC = '\033[0m' # No Color

.PHONY: build
build:
	@echo Building ...
	@go generate ./...
	@go build ./...
	@echo -e ${GREEN}Done${NC}
.PHONY: proto
proto:
	protoc -I raftrpc/ raftrpc/raftrpc.proto --go_out=plugins=grpc:raftrpc

//go:generate protoc -I raftrpc/ raftrpc/raftrpc.proto --go_out=plugins=grpc:raftrpc
package main

import (
	"context"
	"log"
	"net"

	pb "gitlab.com/almeidaraf/raft/raftrpc"
	"google.golang.org/grpc"
)

const (
	port = ":50051"
)

type server struct{}

func (s *server) RequestVote(ctx context.Context, in *pb.RequestVoteIn) (*pb.RequestVoteOut, error) {
	log.Printf("Received RequestVote")
	return &pb.RequestVoteOut{Term: 1, VoteGranted: false}, nil
}

func (s *server) AppendEntries(ctx context.Context, in *pb.AppendEntriesIn) (*pb.AppendEntriesOut, error) {
	log.Printf("Received AppendEntries")
	return &pb.AppendEntriesOut{Term: 1, Success: false}, nil
}

func (s *server) Log(ctx context.Context, in *pb.LogIn) (*pb.LogOut, error) {
	return &pb.LogOut{Commited: true}, nil
}

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterRaftServer(s, &server{})
	log.Printf("Server starting at %s", port)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
